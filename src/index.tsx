import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import MainContextProvider from "./context/MainContext";
import { ApolloProvider } from "@apollo/react-hooks";
import { client } from "./services/ApolloClient";
import { BreakpointProvider } from "react-socks";
import "bootstrap/dist/css/bootstrap.min.css";

ReactDOM.render(
  <ApolloProvider client={client}>
    <BreakpointProvider>
      <MainContextProvider>
        <App />
      </MainContextProvider>
    </BreakpointProvider>
  </ApolloProvider>,
  document.getElementById("root")
);
