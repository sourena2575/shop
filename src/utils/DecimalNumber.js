export const handleDecimalNumber = (number) => {
  let p = number.toString().split(".");
  return p[0]
    .split("")
    .reverse()
    .reduce(function (acc, number, i, orig) {
      return number === "-" ? acc : number + (i && !(i % 3) ? "," : "") + acc;
    }, "");
};
