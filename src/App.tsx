import React, { Suspense, lazy, useContext } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Products from "./containers/Product";
import { CircleProgress } from "./components/Common/Progress";
import AuthModal from "./components/Authentication/Index";
import Navbar from "./components/Common/Navbar";
import SideNav from "./components/Common/Navbar/SideNav";

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <SideNav />
      <AuthModal />
      <Suspense fallback={<CircleProgress center color="primary" />}>
        <Switch>
          <Route exact path="/" component={Products} />
          <Route
            exact
            path="/users"
            component={lazy(() => import("./containers/User"))}
          />

          <Route
            exact
            path="/cart"
            component={lazy(() => import("./containers/Cart"))}
          />
          <Route
            exact
            path="/products/:id"
            component={lazy(() => import("./containers/ProductDetails"))}
          />
          <Route
            exact
            path="/profile"
            component={lazy(() => import("./containers/Profile"))}
          />
          <Route
            exact
            path="/orders/:id"
            component={lazy(() => import("./containers/OrderDetails"))}
          />
          <Route
            exact
            path="/posts"
            component={lazy(() => import("./containers/Posts"))}
          />
        </Switch>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
