import React, { createContext, useReducer } from "react";

export const MainContext = createContext();

const initialState = {
  user: JSON.parse(localStorage.getItem("user")),
  authModal: false,
  authStep: true,
  authError: null,
  cardStep: 0,
  cardItem: {},
  commentModal: false,
  cart: [],
  profileModal: false,
  uploadImage: "",
  addProductModal: false,
  productComment: {},
  userModal: false,
  selsectedUser: null,
  addPostModal: false,
  sideBar: false,
};

const reducer = (mainState = initialState, { type, payload }) => {
  let temp = [...mainState.cart];
  switch (type) {
    case "AUTH_MODAL":
      return {
        ...mainState,
        authModal: !mainState.authModal,
        authError: null,
      };
    case "AUTH_STEP":
      return {
        ...mainState,
        authStep: !mainState.authStep,
        authError: null,
      };
    case "AUTH_ERROR":
      return {
        ...mainState,
        authError: payload,
      };
    case "AUTH_REFRESH":
      window.location = "/";
      return mainState;
    case "CARD_STEP":
      return {
        ...mainState,
        cardStep: payload.step,
        cardItem: payload.cardItem,
      };
    case "COMMENT_MODAL":
      return {
        ...mainState,
        commentModal: !mainState.commentModal,
        productComment: payload ? payload : {},
      };
    case "ADD_CART":
      let item = { ...payload };
      item.count = 1;
      return {
        ...mainState,
        cart: [item, ...mainState.cart],
      };
    case "DELETE_CART":
      return {
        ...mainState,
        cart: [...mainState.cart].filter((item) => item.id !== payload),
      };
    case "INCREMENT_CART":
      temp[temp.findIndex((item) => item.id === payload)].count += 1;
      return {
        ...mainState,
        cart: temp,
      };
    case "DECREMENT_CART":
      temp[temp.findIndex((item) => item.id === payload)].count -= 1;
      return {
        ...mainState,
        cart: temp,
      };
    case "CLEAR_CART":
      return {
        ...mainState,
        cart: [],
      };
    case "PROFILE_MODAL":
      return {
        ...mainState,
        profileModal: !mainState.profileModal,
      };
    case "UPLOAD_IMAGE":
      return {
        ...mainState,
        uploadImage: payload,
      };
    case "ADD_PRODUCT_MODAL":
      return {
        ...mainState,
        addProductModal: !mainState.addProductModal,
      };
    case "USER_MODAL":
      return {
        ...mainState,
        userModal: !mainState.userModal,
        selectedUser: payload ? payload : null,
      };
    case "ADD_POST_MODAL":
      return {
        ...mainState,
        addPostModal: !mainState.addPostModal,
      };

    case "SIDE_BAR":
      return {
        ...mainState,
        sideBar: !mainState.sideBar,
      };
    default:
      return mainState;
  }
};
const MainContextProvider = ({ children }) => {
  const [mainState, mainDispatch] = useReducer(reducer, initialState);
  return (
    <MainContext.Provider value={{ mainState, mainDispatch }}>
      {children}
    </MainContext.Provider>
  );
};

export default MainContextProvider;
