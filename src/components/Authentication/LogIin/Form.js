import React, { useContext } from "react";
import { LoginMutation } from "../../../services/Mutations";
import { DrawerForm } from "../../Common/Form";
import { data } from "./Data.tsx";
import { MainContext } from "../../../context/MainContext";

const LoginForm = () => {
  const { mainDispatch } = useContext(MainContext);
  return (
    <LoginMutation>
      {({ handleSub, loading }) => (
        <DrawerForm
          loading={loading}
          mutation={handleSub}
          data={data}
          close={() => mainDispatch({ type: "AUTH_MODAL" })}
          title="ورود"
          buttonText="ورود"
          changeText=" اگر حساب کاربری ندارید به بخش ثبت نام بروید"
        />
      )}
    </LoginMutation>
  );
};

export default LoginForm;
