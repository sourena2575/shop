export const data = [
  {
    name: "name",
    label: "نام ",
    placeholder: "نام  خود را وارد کنید",
  },
  {
    name: "lastName",
    label: "نام خانوادگی ",
    placeholder: "نام خانوادگی  خود را وارد کنید",
  },
  {
    name: "email",
    label: "ایمیل ",
    placeholder: "ایمیل  خود را وارد کنید",
  },
  {
    name: "userName",
    label: "نام کاربری ",
    placeholder: "نام کاربری  خود را وارد کنید",
  },
  {
    name: "password",
    label: "رمز ورود ",
    placeholder: "رمز ورود خود را وارد کنید",
  },
  {
    name: "password1",
    label: "تکرار رمز ",
    placeholder: "رمز ورود را یک بار دیگر تکرار کنید",
  },
];
