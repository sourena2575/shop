import React, { useContext } from "react";
import { RegisterMutation } from "../../../services/Mutations";
import { DrawerForm } from "../../Common/Form";
import { data } from "./Data";
import { MainContext } from "../../../context/MainContext";

const RegisterForm = () => {
  const { mainDispatch } = useContext(MainContext);
  return (
    <RegisterMutation>
      {({ handleSub, loading }) => (
        <DrawerForm
          mutation={handleSub}
          loading={loading}
          data={data}
          close={() => mainDispatch({ type: "AUTH_MODAL" })}
          title="ثبت نام"
          buttonText="ثبت نام"
          changeText=" اگر حساب کاربری دارید به بخش ورود بروید"
        />
      )}
    </RegisterMutation>
  );
};

export default RegisterForm;
