import React, { useContext } from "react";
import { Drawer } from "@material-ui/core";
import { MainContext } from "../../context/MainContext";
import { RegisterForm } from "./Register";
import { LoginForm } from "./LogIin";
const AuthModal = () => {
  const { mainState, mainDispatch } = useContext(MainContext);

  return (
    <Drawer
      anchor="right"
      className="text-center"
      onClose={() => mainDispatch({ type: "AUTH_MODAL" })}
      open={mainState.authModal}
    >
      {mainState.authStep ? <LoginForm /> : <RegisterForm />}
    </Drawer>
  );
};

export default AuthModal;
