import React, { useContext, useEffect } from "react";
import { useLazyQuery } from "@apollo/react-hooks";
import { Dialog, DialogContent, DialogTitle } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { MainContext } from "../../../context/MainContext";
import CommentInput from "./CommentInput.js";
import CommentBox from "./CommentBox.js";
import { PRODUCT } from "../../../services/queries/Products";
import { CircleProgress } from "../../Common/Progress";

const CommentModal = () => {
  const { mainState, mainDispatch } = useContext(MainContext);
  const [getProduct, { loading, called, data }] = useLazyQuery(PRODUCT, {
    variables: { productId: mainState.productComment.id },
  });

  useEffect(() => {
    if (mainState.commentModal) {
      getProduct();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mainState.commentModal]);

  return (
    <Dialog
      open={mainState.commentModal}
      onClose={() => mainDispatch({ type: "COMMENT_MODAL" })}
    >
      <DialogTitle id="alert-dialog-slide-title">
        {called && loading ? (
          <div className="flex justify-center align-middle">
            <CircleProgress color="secondary" center="" />
          </div>
        ) : (
          <CloseIcon
            className=" mt-2"
            onClick={() => mainDispatch({ type: "COMMENT_MODAL" })}
            style={{ cursor: "pointer", fontSize: 20 }}
          />
        )}
      </DialogTitle>
      <DialogContent>
        {data && (
          <>
            <CommentInput item={data.getProduct} />
            <CommentBox item={data.getProduct} />
          </>
        )}
      </DialogContent>
    </Dialog>
  );
};

export default CommentModal;
