import React, { useContext } from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import { Divider } from "@material-ui/core";
import { Col } from "reactstrap";
import moment from "moment";
import { DeleteCommentMutation } from "../../../services/Mutations";
import { MainContext } from "../../../context/MainContext";

const CommentBox = ({ item }) => {
  const { mainState } = useContext(MainContext);
  return (
    <Col
      xs="12"
      className="mt-2 border rounded w-100 mb-4 "
      // style={{ height: 200, overflowY: "scroll", overflowX: "hidden" }}
    >
      {item.comments.map((it, index) => {
        return (
          <div key={it.id} className="row py-2">
            <div className="col-3 d-flex flex-column">
              {mainState.user && it.userName === mainState.user.userName && (
                <DeleteCommentMutation>
                  {({ handleDelete, loading }) => (
                    <DeleteIcon
                      className="text-danger mt-2"
                      style={{ cursor: "pointer", opacity: loading ? 0.3 : 1 }}
                      onClick={() => handleDelete(item.id, it.id)}
                    />
                  )}
                </DeleteCommentMutation>
              )}
              <span
                className="text-center text-info pt-2"
                style={{ fontSize: 8 }}
              >
                {moment(it.createdAt).fromNow()}
              </span>
            </div>
            <div className="col-9 d-flex flex-column">
              <p className="text-right pr-1" style={{ fontSize: 12 }}>
                {it.body}
              </p>
              <span className="text-left text-muted " style={{ fontSize: 10 }}>
                {it.userName}
              </span>
            </div>
            {index !== item.comments.length - 1 && (
              <div className="col-12 mx-auto pt-3">
                <Divider />
              </div>
            )}
          </div>
        );
      })}
    </Col>
  );
};

export default CommentBox;
