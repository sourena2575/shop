import React, { useContext, useEffect } from "react";
import { useLazyQuery } from "@apollo/react-hooks";
import { Dialog, DialogContent, DialogTitle } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import CommentInput from "./CommentInput.js";
import CommentBox from "./CommentBox.js";
import { MainContext } from "../../../context/MainContext";
import { POST } from "../../../services/queries/Posts.js";
import { CircleProgress } from "../../Common/Progress/index.js";

const CommentModal = () => {
  const { mainState, mainDispatch } = useContext(MainContext);
  const [getPost, { loading, called, data }] = useLazyQuery(POST, {
    variables: { postId: mainState.productComment.id },
  });

  useEffect(() => {
    if (mainState.commentModal) {
      getPost();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mainState.commentModal]);

  return (
    <Dialog
      open={mainState.commentModal}
      onClose={() => mainDispatch({ type: "COMMENT_MODAL" })}
    >
      <DialogTitle id="alert-dialog-slide-title">
        {called && loading ? (
          <div className="flex justify-center align-middle">
            <CircleProgress color="secondary" center="" />
          </div>
        ) : (
          <CloseIcon
            className=" mt-2"
            onClick={() => mainDispatch({ type: "COMMENT_MODAL" })}
            style={{ cursor: "pointer", fontSize: 20 }}
          />
        )}
      </DialogTitle>
      <DialogContent>
        {data && (
          <>
            <CommentInput item={data.getPost} />
            <CommentBox item={data.getPost} />
          </>
        )}
      </DialogContent>
    </Dialog>
  );
};

export default CommentModal;
