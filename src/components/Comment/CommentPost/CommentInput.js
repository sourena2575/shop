import React, { useState } from "react";
import { Col, Row, Input } from "reactstrap";
import { Button } from "@material-ui/core";
import { CommentPostMutation } from "../../../services/Mutations";

const CommentInput = ({ item }) => {
  const [body, setbody] = useState("");
  const postId = item.id;

  return (
    <CommentPostMutation>
      {({ handleComment, loading }) => {
        return (
          <Row>
            <Col xs="3">
              <Button
                disabled={loading}
                size="medium"
                variant="contained"
                color="primary"
                onClick={
                  body !== ""
                    ? () => {
                        handleComment(postId, body);
                        setbody("");
                      }
                    : null
                }
              >
                ارسال
              </Button>
            </Col>
            <Col xs="9">
              <Input
                type="textarea"
                placeholder="نظر خود را وارد کنید"
                className="text-right"
                onChange={(e) => setbody(e.target.value)}
                value={body}
              />
            </Col>
          </Row>
        );
      }}
    </CommentPostMutation>
  );
};

export default CommentInput;
