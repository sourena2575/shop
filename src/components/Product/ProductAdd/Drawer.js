import React, { useContext } from "react";
import { Drawer } from "@material-ui/core";
import { MainContext } from "../../../context/MainContext";
import { DrawerForm } from "../../Common/Form";
import { FormData } from "./FormData.tsx";
import { AddProductMutation } from "../../../services/Mutations";
const AddProductModal = () => {
  const { mainDispatch, mainState } = useContext(MainContext);
  return (
    <Drawer
      anchor="left"
      className="text-center"
      onClose={() => mainDispatch({ type: "ADD_PRODUCT_MODAL" })}
      open={mainState.addProductModal}
    >
      <AddProductMutation>
        {({ handleCreateProduct, loading }) => (
          <DrawerForm
            data={FormData}
            loading={loading}
            upload
            buttonText="تایید"
            mutation={handleCreateProduct}
            close={() => mainDispatch({ type: "ADD_PRODUCT_MODAL" })}
            title="ایجاد محصول"
          />
        )}
      </AddProductMutation>
    </Drawer>
  );
};

export default AddProductModal;
