export const FormData = [
  {
    name: "title",
    label: " عنوان محصول  ",
    placeholder: "عنوان محصول را وارد کنید",
  },
  {
    name: "company",
    label: "شرکت  ",
    placeholder: "شزکت تولید کننده را وارد کنید",
  },
  {
    name: "price",
    label: "قیمت   ",
    placeholder: "قیمت محصول را وارد ",
  },
  {
    name: "color",
    label: "رنگ ",
    placeholder: "رنگ محصول را وارد کنید",
  },
  {
    name: "size",
    label: "اندازه ",
    placeholder: "اندازه محصول را وارد کنید",
  },
  {
    name: "number",
    label: "تعداد ",
    placeholder: "تعداد محصول را وارد کنید",
  },
  {
    name: "desc",
    label: "توضیحات ",
    placeholder: "توضیحات محصول را وارد کنید",
    textArea: true,
  },
];
