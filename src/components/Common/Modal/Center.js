import React, { useContext } from "react";
import { MainContext } from "../../../context/MainContext";

const CenterModal = ({ children }) => {
  const { mainState, mainDispatch } = useContext(MainContext);
  return (
    <>
      <div
        className="fixed top-0 right-0 bottom-0 left-0 w-full h-full bg-black opacity-50  cursor-default"
        hidden={!mainState.modal}
        onClick={() => mainDispatch({ type: "MODAL" })}
      ></div>
      <div
        className=" w-11/12 md:w-3/4 h-auto mx-auto mt-24 rounded bg-white transition-all relative overflow-hidden"
        hidden={!mainState.modal}
      >
        <div className="absolute top-0 right-0 bottom-0 left-0 w-full h-full">
          {children}
        </div>
      </div>
    </>
  );
};
export default CenterModal;
