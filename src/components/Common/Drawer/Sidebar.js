import React, { useContext } from "react";
import MenuIcon from "@material-ui/icons/Menu";
import { MainContext } from "../../../context/MainContext";

const RightDrawer = () => {
  const { mainState, mainDispatch } = useContext(MainContext);
  return (
    <div
      className={
        mainState.sideBar
          ? " bg-white w-76 h-full absolute top-0 right-0 shadow z-10"
          : " -mr-76 bg-white w-76 h-full absolute top-0 right-0 shadow z-10"
      }
    >
      <button
        className=" text-gray-700 float-left m-4 focus:outline-none focus:text-gray-600"
        onClick={() => mainDispatch({ type: "SIDE_BAR" })}
      >
        <MenuIcon style={{ fontSize: 40 }} />
      </button>
    </div>
  );
};

export default RightDrawer;
