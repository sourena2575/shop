import { useState, useContext } from "react";
import axios from "axios";
import { MainContext } from "../../../context/MainContext";

const Uploader = ({ children }) => {
  const { mainDispatch } = useContext(MainContext);
  const [file, setfile] = useState(null);
  const [loading, setloading] = useState(false);

  const handleUpload = () => {
    const data = new FormData();
    data.append("file", file);
    data.append("upload_preset", "xicq5r2v");
    axios
      .post(" https://api.cloudinary.com/v1_1/dqn0ufzhz/image/upload", data)
      .then((res) => {
        mainDispatch({ type: "UPLOAD_IMAGE", payload: res.data.secure_url });
        setloading(false);
      })
      .catch((er) => {
        console.log(er);
      });
  };

  return children({
    setfile: setfile,
    loading: loading,
    handleUpload: handleUpload,
    file: file,
    setloading: setloading,
  });
};

export default Uploader;
