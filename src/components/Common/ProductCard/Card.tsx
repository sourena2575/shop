import React, { FC } from "react";
import CardBody from "./CardBody";
import CardImage from "./CardImage";
interface Props {
  item: any;
  cart: boolean;
  orderId: string | null;
}
const Card: FC<Props> = ({ item, orderId, cart }) => {
  return (
    <div className="mx-auto">
      <CardImage item={item} />
      <CardBody item={item} orderId={orderId} cart={cart} />
    </div>
  );
};
export default Card;
