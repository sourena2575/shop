import React from "react";

import {
  LikeIcon,
  AddCartIcon,
  InfoButton,
  AddCommentIcon,
  DecButton,
  IncButton,
  DelButton,
  DelOrderButton,
} from "../Button";

interface Props {
  item: {
    id: string;
  };
  cart: boolean;
  orderId: string | null;
  post: boolean;
}
const CardButtons: React.FC<Props> = ({ item, cart, orderId, post }) => {
  return (
    <div className="flex-row p-2">
      {cart ? (
        <>
          <DecButton item={item} />
          <DelButton item={item} />
          <IncButton item={item} />
          <InfoButton item={item} />
        </>
      ) : (
        <>
          <LikeIcon item={item} post={post} />
          <AddCommentIcon item={item} />
          {!orderId && <AddCartIcon item={item} />}
          {orderId ? (
            <DelOrderButton productId={item.id} orderId={orderId} />
          ) : (
            <InfoButton item={item} />
          )}
        </>
      )}
    </div>
  );
};

export default CardButtons;
