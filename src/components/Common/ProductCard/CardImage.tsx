import React, { FC } from "react";
interface Props {
  item: {
    image: string;
    title: string;
  };
}
const CardImage: FC<Props> = ({ item }) => {
  return (
    <div className=" overflow-hidden">
      <img
        src={item.image}
        alt={item.title}
        className="object-cover w-74 h-64 right-0 left-0 top-0 bottom-0 rounded"
      />
    </div>
  );
};

export default CardImage;
