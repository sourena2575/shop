import React, { FC } from "react";
import CardButton from "./CardButton";
import { handlePersianNumber } from "../../../utils/PersianNumber";
import { handleDecimalNumber } from "../../../utils/DecimalNumber";

interface Props {
  orderId: string | null;
  cart: boolean;
  item: {
    price: number;
    desc: string;
    title: string;
    id: string;
  };
}
const CardBody: FC<Props> = ({ item, orderId, cart }) => {
  return (
    <div className=" bg-white -mt-24 relative rounded w-64 mx-auto shadow">
      <div className="flex flex-row justify-between pt-3 px-2">
        <span className="text-teal-600 pl-2">
          {handlePersianNumber(handleDecimalNumber(item.price))} $
        </span>
        <span className="font-bold">{item.title}</span>
      </div>
      <div className="text-right text-gray-600 p-2 text-xs truncate">
        {item.desc}
      </div>
      <CardButton item={item} cart={cart} orderId={orderId} post={false} />
    </div>
  );
};

export default CardBody;
