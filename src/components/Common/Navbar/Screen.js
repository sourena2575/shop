import React, { useContext } from "react";
import { NavLink, useLocation } from "react-router-dom";
import { MainContext } from "../../../context/MainContext";

const Screen = () => {
  const { mainState, mainDispatch } = useContext(MainContext);
  const location = useLocation();
  return (
    <div className=" flex-row justify-end py-6 hidden md:block pr-8 ">
      {location.pathname === "/posts" && mainState.user && (
        <button
          className="ml-3 rounded px-2 py-1 bg-teal-600 text-white"
          onClick={() => mainDispatch({ type: "ADD_POST_MODAL" })}
        >
          ایجاد پست
        </button>
      )}
      {location.pathname === "/" && mainState.user && (
        <button
          className="ml-3 rounded px-2 py-1 bg-teal-600  text-white"
          onClick={() => mainDispatch({ type: "ADD_PRODUCT_MODAL" })}
        >
          ایجاد محصول
        </button>
      )}
      {mainState.user ? (
        <>
          <button
            className="ml-3 rounded px-2 py-1 bg-red-600 text-white "
            onClick={() => {
              localStorage.removeItem("user");
              window.location = "/";
            }}
          >
            خروج
          </button>
          <NavLink to="/profile" className="pl-3 text-blue-500">
            {mainState.user.userName}
          </NavLink>
        </>
      ) : (
        <button
          className="ml-3 rounded px-2 py-1 bg-blue-500 "
          onClick={() => mainDispatch({ type: "AUTH_MODAL" })}
        >
          ورود
        </button>
      )}
      <NavLink to="/cart" className="pl-3 text-yellow-600">
        سبد خرید
      </NavLink>
      <NavLink to="/users" className="pl-3 text-pink-500">
        کاربران
      </NavLink>
      <NavLink to="/posts" className="pl-3 text-teal-600">
        فید
      </NavLink>
    </div>
  );
};

export default Screen;
