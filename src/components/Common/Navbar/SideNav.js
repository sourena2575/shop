import React, { useContext } from "react";
import { MainContext } from "../../../context/MainContext";
import Mobile from "./Mobile";
import Toggle from "./Toggle";

const RightDrawer = () => {
  const { mainState } = useContext(MainContext);
  return (
    <div
      className=" bg-white w-76 h-full absolute top-0 right-0 shadow z-10 "
      hidden={!mainState.sideBar}
    >
      <Toggle side />
      <Mobile />
    </div>
  );
};

export default RightDrawer;
