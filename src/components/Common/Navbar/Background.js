import React, { useContext } from "react";
import { MainContext } from "../../../context/MainContext";

const Background = () => {
  const { mainState, mainDispatch } = useContext(MainContext);
  return (
    <button
      className="fix absolute inset-0 w-full h-full z-0 bg-black opacity-50"
      hidden={!mainState.sideBar}
      onClick={() => mainDispatch({ type: "SIDE_BAR" })}
    ></button>
  );
};

export default Background;
