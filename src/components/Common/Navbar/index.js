import React from "react";
import { NavLink } from "react-router-dom";
import Toggle from "./Toggle";
import Background from "./Background";
import Screen from "./Screen";

const Index = () => {
  return (
    <>
      <div className=" bg-indigo-900 shadow ab inset-0 h-20 flex flex-row justify-between px-10 relative">
        <Toggle />
        <NavLink to="/" className=" text-white text-2xl p-6">
          خانه
        </NavLink>
        <Screen />
      </div>
      <Background />
    </>
  );
};

export default Index;
