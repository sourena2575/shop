import React, { useContext } from "react";
import MenuIcon from "@material-ui/icons/Menu";
import { MainContext } from "../../../context/MainContext";

const Toggle = ({ side }) => {
  const { mainDispatch } = useContext(MainContext);
  return (
    <button
      className="  text-gray-600 focus:outline-none focus:text-gray-600 block md:hidden absolute"
      onClick={() => mainDispatch({ type: "SIDE_BAR" })}
      style={{ top: side ? 30 : 20, right: side ? 240 : 20 }}
    >
      <MenuIcon style={{ fontSize: 40 }} />
    </button>
  );
};

export default Toggle;
