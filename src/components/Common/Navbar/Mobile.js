import React, { useContext } from "react";
import { NavLink, useLocation } from "react-router-dom";
import { MainContext } from "../../../context/MainContext";

const Mobile = () => {
  const { mainState, mainDispatch } = useContext(MainContext);
  const location = useLocation();
  return (
    <div className="flex flex-col items-end pr-5 pt-8">
      {location.pathname === "/posts" && mainState.user && (
        <button
          className="ml-3 rounded px-2 py-1 bg-teal-600 text-white my-2"
          onClick={() => mainDispatch({ type: "ADD_POST_MODAL" })}
        >
          ایجاد پست
        </button>
      )}
      {location.pathname === "/" && mainState.user && (
        <button
          className="ml-3 rounded px-2 py-1 bg-teal-600 text-white my-2"
          onClick={() => mainDispatch({ type: "ADD_PRODUCT_MODAL" })}
        >
          ایجاد محصول
        </button>
      )}
      {mainState.user ? (
        <>
          <button
            className="ml-3 rounded px-2 py-1 bg-red-600 text-white my-2"
            onClick={() => {
              localStorage.removeItem("user");
              window.location = "/";
            }}
          >
            خروج
          </button>
          <NavLink to="/profile" className="pl-3 text-blue-500 my-2">
            {mainState.user.userName}
          </NavLink>
        </>
      ) : (
        <button
          className="ml-3 rounded px-2 py-1 bg-blue-500 my-2"
          onClick={() => mainDispatch({ type: "AUTH_MODAL" })}
        >
          ورود
        </button>
      )}
      <NavLink to="/cart" className="pl-3 text-yellow-600 my-2">
        سبد خرید
      </NavLink>
      <NavLink to="/users" className="pl-3 text-pink-500 my-2">
        کاربران
      </NavLink>
      <NavLink to="/posts" className="pl-3 text-teal-600 my-2">
        فید
      </NavLink>
    </div>
  );
};

export default Mobile;
