import React from "react";

const CustomInput = ({ label, placeholder, id, name }) => {
  return (
    <div>
      <label htmlFor={id} className=" float-right pr-2 text-gray-700">
        {label}
      </label>
      <input
        className="border rounded p-2 text-right w-full"
        placeholder={placeholder}
        id={id}
        name={name}
      />
    </div>
  );
};

export default CustomInput;
