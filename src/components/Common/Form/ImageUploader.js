import React, { useContext } from "react";
import { Grid, IconButton } from "@material-ui/core";
import { Input, Label } from "reactstrap";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import PublishIcon from "@material-ui/icons/Publish";
import { Uploader } from "../Functions";
import { MainContext } from "../../../context/MainContext";
import { CircleProgress } from "../Progress";

const UploadFile = () => {
  const { mainState } = useContext(MainContext);
  return (
    <Uploader>
      {({ setfile, loading, handleUpload, file, setloading }) => {
        if (loading) {
          return (
            <Grid item xs={12}>
              <CircleProgress color="secondary" />
            </Grid>
          );
        } else {
          return (
            <>
              <Grid item xs={12}>
                <PublishIcon
                  size="small"
                  color="secondary"
                  hidden={file ? false : true}
                  onClick={() => {
                    handleUpload();
                    setloading(true);
                  }}
                />

                <Input
                  id="icon-button-file"
                  type="file"
                  name="file"
                  onChange={(e) => setfile(e.target.files[0])}
                  hidden={true}
                />
                <Label htmlFor="icon-button-file">
                  <IconButton
                    color="secondary"
                    aria-label="upload picture"
                    component="span"
                  >
                    <PhotoCamera />
                  </IconButton>
                </Label>
              </Grid>
              {mainState.uploadImage && (
                <Grid item xs={12} className="my-2">
                  <img
                    className="img-fluid rounded"
                    src={mainState.uploadImage}
                    alt="a"
                    style={{ width: 290, height: 230 }}
                  />
                </Grid>
              )}
            </>
          );
        }
      }}
    </Uploader>
  );
};

export default UploadFile;
