import React, { useContext } from "react";
import ImageUploader from "./ImageUploader";
import CustomInput from "../Input/CustomInput";
import FormButtons from "./FormButtons";
import FormHeader from "./FormHeader";
import { MainContext } from "../../../context/MainContext";
import FormAlert from "./FormAlert";
interface Props {
  data: [
    {
      name: string;
      label: string;
      placeholder: string;
    }
  ];
  buttonText: string;
  changeText: string;
  loading: boolean;
  upload: true;
  mutation: any;
  title: string;
  close: any;
}

const DrawerForm: React.FC<Props> = ({
  data,
  buttonText,
  changeText,
  loading,
  upload,
  mutation,
  title,
  close,
}) => {
  const { mainState } = useContext(MainContext);
  return (
    <form onSubmit={mutation}>
      <div className="grid grid:cols-1 gap-2 px-2">
        <FormHeader close={close} title={title} />
        {data.map((it, index) => {
          return (
            <div className="col-span-1" key={index}>
              <CustomInput
                id={it.name}
                label={it.label}
                name={it.name}
                placeholder={it.placeholder}
              />
            </div>
          );
        })}
        {upload && (
          <div className="col-span-1">
            <ImageUploader />
          </div>
        )}
        {mainState.authError && <FormAlert error={mainState.authError} />}
        <FormButtons
          loading={loading}
          buttonText={buttonText}
          changeText={changeText}
        />
      </div>
    </form>
  );
};

export default DrawerForm;
