import React, { FC, useContext } from "react";
import { MainContext } from "../../../context/MainContext";
interface Props {
  loading: boolean;
  buttonText: string;
  changeText: string;
}
const FormButtons: FC<Props> = ({ loading, buttonText, changeText }) => {
  const { mainDispatch } = useContext(MainContext);
  return (
    <>
      <div className="col-span-1">
        <button
          className=" p-2 w-full text-white bg-indigo-700 rounded-lg my-2"
          type="submit"
          disabled={loading}
          style={{ opacity: loading ? 0.3 : 1 }}
        >
          {buttonText}
        </button>
      </div>
      <div className="col-span-1">
        <button
          className=" w-full text-indigo-700 text-sm rounded-lg my-2"
          onClick={() => mainDispatch({ type: "AUTH_STEP" })}
        >
          {changeText}
        </button>
      </div>
    </>
  );
};

export default FormButtons;
