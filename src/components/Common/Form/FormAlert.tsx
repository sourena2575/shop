import React, { FC } from "react";

interface Props {
  error: string | null;
}
const FormAlert: FC<Props> = ({ error }) => {
  return (
    <div className="px-2 bg-red-400 rounded-lg w-full flex items-center justify-center">
      <p className=" text-center text-red-700 py-2">{error}</p>
    </div>
  );
};

export default FormAlert;
