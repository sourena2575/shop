import React, { FC } from "react";
import CloseIcon from "@material-ui/icons/Close";

interface Props {
  close: any;
  title: string;
}

const FormHeader: FC<Props> = ({ close, title }) => {
  return (
    <div className="flex flex-row justify-between px-3">
      <CloseIcon
        className=" mt-3"
        onClick={close}
        style={{ cursor: "pointer", fontSize: 20 }}
      />
      <p className="text-right font-bold text-indigo-600 pt-3"> {title}</p>
    </div>
  );
};

export default FormHeader;
