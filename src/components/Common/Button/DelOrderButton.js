import React from "react";
import { IconButton } from "@material-ui/core";
import { DeleteOrderMutation } from "../../../services/Mutations";
import DeleteIcon from "@material-ui/icons/Delete";

const DelOrderButton = ({ productId, orderId }) => {
  return (
    <DeleteOrderMutation>
      {({ handleDelete, loading }) => (
        <IconButton onClick={() => handleDelete(productId, orderId)}>
          <DeleteIcon
            style={{
              opacity: loading ? 0.3 : 1,
              cursor: "pointer",
            }}
            className="text-danger"
          />
        </IconButton>
      )}
    </DeleteOrderMutation>
  );
};

export default DelOrderButton;
