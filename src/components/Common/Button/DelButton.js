import React, { useContext } from "react";
import { IconButton } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import Badge from "@material-ui/core/Badge";
import { MainContext } from "../../../context/MainContext";

const DelButton = ({ item }) => {
  const { mainDispatch } = useContext(MainContext);

  return (
    <IconButton
      component="span"
      onClick={() => mainDispatch({ type: "DELETE_CART", payload: item.id })}
    >
      <Badge badgeContent={item.count} className=" text-red-700 p-2">
        <DeleteIcon className="text-danger" />
      </Badge>
    </IconButton>
  );
};

export default DelButton;
