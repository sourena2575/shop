import React, { useContext } from "react";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { IconButton, Badge } from "@material-ui/core";
import { MainContext } from "../../../context/MainContext";
const AddCartIcon = ({ item }) => {
  const { mainState, mainDispatch } = useContext(MainContext);
  const find = mainState.cart.find((it) => it.id === item.id);
  return (
    <IconButton
      className="mr-2"
      component="span"
      onClick={
        find
          ? () => mainDispatch({ type: "DELETE_CART", payload: item.id })
          : () => mainDispatch({ type: "ADD_CART", payload: item })
      }
    >
      {find ? (
        <Badge
          badgeContent={mainState.cart.length}
          className=" text-yellow-600 p-1"
        >
          <ShoppingCartIcon className="text-warning" />
        </Badge>
      ) : (
        <AddShoppingCartIcon className="text-warning" />
      )}
    </IconButton>
  );
};

export default AddCartIcon;
