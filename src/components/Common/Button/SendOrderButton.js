import React from "react";
import RotateLeftIcon from "@material-ui/icons/RotateLeft";
import LocalShippingIcon from "@material-ui/icons/LocalShipping";
import { SendOrderMutation } from "../../../services/Mutations";
const SendOrderButton = (item) => {
  return (
    <SendOrderMutation>
      {({ handleSend, loading }) => {
        if (item.item.status === "ارسال شد") {
          return (
            <LocalShippingIcon
              className="text-success"
              style={{ cursor: "pointer" }}
            />
          );
        } else {
          return (
            <RotateLeftIcon
              className="text-warning"
              onClick={item.item.payed ? () => handleSend(item.item.id) : null}
              style={{ opacity: loading ? 0.3 : 1, cursor: "pointer" }}
            />
          );
        }
      }}
    </SendOrderMutation>
  );
};

export default SendOrderButton;
