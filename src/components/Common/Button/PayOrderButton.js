import React from "react";
import MoneyOffIcon from "@material-ui/icons/MoneyOff";
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import { PayOrderMutation } from "../../../services/Mutations";
const PayOrderButton = (item) => {
  return (
    <PayOrderMutation>
      {({ handlePay, loading }) => {
        if (item.item.payed) {
          return (
            <MonetizationOnIcon
              className="text-success"
              style={{ cursor: "pointer" }}
            />
          );
        } else {
          return (
            <MoneyOffIcon
              className="text-warning"
              onClick={() => handlePay(item.item.id)}
              style={{ opacity: loading ? 0.3 : 1, cursor: "pointer" }}
            />
          );
        }
      }}
    </PayOrderMutation>
  );
};

export default PayOrderButton;
