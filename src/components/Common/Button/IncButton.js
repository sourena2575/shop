import React, { useContext } from "react";
import { IconButton } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import { MainContext } from "../../../context/MainContext";

const IncButton = ({ item }) => {
  const { mainDispatch } = useContext(MainContext);

  return (
    <IconButton
      component="span"
      onClick={() => mainDispatch({ type: "INCREMENT_CART", payload: item.id })}
    >
      <AddIcon className="text-primary" />
    </IconButton>
  );
};

export default IncButton;
