import React from "react";
import { DeleteOrdersMutation } from "../../../services/Mutations";
import DeleteIcon from "@material-ui/icons/Delete";

const DelOrdersButton = ({ orderId }) => {
  return (
    <DeleteOrdersMutation>
      {({ handleDelete, loading }) => (
        <DeleteIcon
          style={{
            opacity: loading ? 0.3 : 1,
            fontSize: 30,
            cursor: "pointer",
          }}
          className="text-danger"
          onClick={() => handleDelete(orderId)}
        />
      )}
    </DeleteOrdersMutation>
  );
};

export default DelOrdersButton;
