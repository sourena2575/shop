import React, { useContext } from "react";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import { IconButton } from "@material-ui/core";
import RemoveIcon from "@material-ui/icons/Remove";
import { FollowMutation } from "../../../services/Mutations";
import { MainContext } from "../../../context/MainContext";

const FollowButton = ({ item }) => {
  const { mainState } = useContext(MainContext);
  const find = item.followers.find((it) => it === mainState.user.id);
  return (
    <FollowMutation>
      {({ handleFollow, loading }) => (
        <IconButton
          component="span"
          onClick={() => handleFollow(item.id)}
          className=" float-right mr-3"
        >
          {find ? (
            <RemoveIcon
              style={{ opacity: loading ? 0.3 : 1 }}
              className=" text-orange-600"
            />
          ) : (
            <AddCircleIcon
              style={{ opacity: loading ? 0.3 : 1 }}
              className="text-indigo-700 "
            />
          )}
        </IconButton>
      )}
    </FollowMutation>
  );
};

export default FollowButton;
