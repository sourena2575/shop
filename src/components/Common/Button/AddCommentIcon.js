import React, { useContext } from "react";
import { Badge, IconButton } from "@material-ui/core";
import ChatIcon from "@material-ui/icons/Chat";
import { MainContext } from "../../../context/MainContext";
const AddCommentIcon = ({ item }) => {
  const { mainDispatch } = useContext(MainContext);
  return (
    <IconButton
      component="span"
      onClick={() => mainDispatch({ type: "COMMENT_MODAL", payload: item })}
    >
      <Badge badgeContent={item.commentCount} className="text-teal-600 p-1">
        <ChatIcon style={{ cursor: "pointer" }} className="text-success" />
      </Badge>
    </IconButton>
  );
};

export default AddCommentIcon;
