import React from "react";
import { IconButton } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { DeletePostMutation } from "../../../services/Mutations";

const DelButton = ({ item }) => {
  return (
    <DeletePostMutation>
      {({ handleDelete, loading }) => (
        <IconButton component="span" onClick={() => handleDelete(item.id)}>
          <DeleteIcon
            className="text-danger"
            style={{ opacity: loading ? 0.3 : 1 }}
          />
        </IconButton>
      )}
    </DeletePostMutation>
  );
};

export default DelButton;
