import React, { useContext } from "react";
import { IconButton } from "@material-ui/core";
import RemoveIcon from "@material-ui/icons/Remove";
import { MainContext } from "../../../context/MainContext";
const DecButton = ({ item }) => {
  const { mainDispatch } = useContext(MainContext);
  return (
    <IconButton
      component="span"
      onClick={
        item.count === 1
          ? () => mainDispatch({ type: "DELETE_CART", payload: item.id })
          : () =>
              mainDispatch({
                type: "DECREMENT_CART",
                payload: item.id,
              })
      }
    >
      <RemoveIcon className="text-warning" />
    </IconButton>
  );
};

export default DecButton;
