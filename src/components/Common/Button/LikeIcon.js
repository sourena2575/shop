import React, { useContext } from "react";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import FavoriteIcon from "@material-ui/icons/Favorite";
import { IconButton, Badge } from "@material-ui/core";
import {
  LikeProductMutation,
  LikePostMutation,
} from "../../../services/Mutations";
import { MainContext } from "../../../context/MainContext";

const LikeIcon = ({ item, post }) => {
  const { mainState } = useContext(MainContext);
  const find =
    mainState.user &&
    item.likes.find((it) => it.userName === mainState.user.userName);
  if (post) {
    return (
      <LikePostMutation>
        {({ handleLike, loading }) => (
          <IconButton
            color={find ? "secondary" : "primary"}
            component="span"
            onClick={() => handleLike(item.id)}
          >
            <Badge badgeContent={item.likeCount} className="text-red-700 p-1">
              {find ? (
                <FavoriteIcon
                  style={{ opacity: loading ? 0.3 : 1 }}
                  className="text-red-600"
                />
              ) : (
                <ThumbUpIcon
                  style={{ opacity: loading ? 0.3 : 1 }}
                  className="text-teal-600"
                />
              )}
            </Badge>
          </IconButton>
        )}
      </LikePostMutation>
    );
  }
  return (
    <LikeProductMutation>
      {({ handleLike, loading }) => (
        <IconButton
          color={find ? "secondary" : "primary"}
          component="span"
          onClick={() => handleLike(item.id)}
        >
          <Badge badgeContent={item.likeCount} className="text-red-700 p-1">
            {find ? (
              <FavoriteIcon
                style={{ opacity: loading ? 0.3 : 1 }}
                className="text-red-600"
              />
            ) : (
              <ThumbUpIcon
                style={{ opacity: loading ? 0.3 : 1 }}
                className="text-teal-600"
              />
            )}
          </Badge>
        </IconButton>
      )}
    </LikeProductMutation>
  );
};

export default LikeIcon;
