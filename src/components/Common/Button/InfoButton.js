import React from "react";
import { IconButton } from "@material-ui/core";
import InfoIcon from "@material-ui/icons/Info";
import { Link } from "react-router-dom";
const InfoButton = ({ item }) => {
  return (
    <Link to={"/products/" + item.id}>
      <IconButton>
        <InfoIcon className="text-info" />
      </IconButton>
    </Link>
  );
};

export default InfoButton;
