import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
const CircleProgress = ({ color, center }) => {
  return (
    <div
      className={
        center && "d-flex justify-content-center align-items-center py-5 my-5"
      }
    >
      <CircularProgress color={color} />
    </div>
  );
};

export default CircleProgress;
