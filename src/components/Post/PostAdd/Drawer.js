import React, { useContext } from "react";
import { Drawer } from "@material-ui/core";
import { MainContext } from "../../../context/MainContext";
import { DrawerForm } from "../../Common/Form";
import { FormData } from "./FormData.tsx";
import { AddPostMutation } from "../../../services/Mutations";
const AddProductModal = () => {
  const { mainDispatch, mainState } = useContext(MainContext);
  return (
    <Drawer
      anchor="left"
      className="text-center"
      onClose={() => mainDispatch({ type: "ADD_POST_MODAL" })}
      open={mainState.addPostModal}
    >
      <AddPostMutation>
        {({ handleCreatePost, loading }) => (
          <DrawerForm
            data={FormData}
            loading={loading}
            upload
            buttonText="تایید"
            mutation={handleCreatePost}
            close={() => mainDispatch({ type: "ADD_POST_MODAL" })}
            title="ایجاد محتوا"
          />
        )}
      </AddPostMutation>
    </Drawer>
  );
};

export default AddProductModal;
