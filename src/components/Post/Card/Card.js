import React from "react";
import CardBody from "./CardBody";
import CardButtons from "./CardButtons";
import CommentModal from "../../Comment/CommentPost/CommentModal";

const Card = ({ item }) => {
  return (
    <div className="mx-auto">
      <CardBody item={item} />
      <CardButtons item={item} />
      <CommentModal />
    </div>
  );
};

export default Card;
