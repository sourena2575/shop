import React from "react";
import { LikeIcon, AddCommentIcon, DelPostButton } from "../../Common/Button";

const CardButtons = ({ item }) => {
  return (
    <div className=" bg-gray-900 rounded-lg w-48 -mt-10 mx-auto h-16 flex flex-row justify-around items-center relative">
      <LikeIcon item={item} post={true} />
      <AddCommentIcon item={item} post={true} />
      <DelPostButton item={item} />
    </div>
  );
};

export default CardButtons;
