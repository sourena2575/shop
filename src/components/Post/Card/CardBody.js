import React from "react";

const CardBody = ({ item }) => {
  return (
    <div className="relative">
      <img
        src={item.image}
        alt="تصویر"
        className="object-cover w-74 h-40 right-0 left-0 top-0 bottom-0 rounded "
      />
      <div className=" bg-teal-700 absolute top-0 right-0 rounded w-54 h-24 p-1 shadow-xl overflow-hidden">
        <p className="text-right  text-gray-400 font-bold">{item.userName}</p>
        <p className="text-right text-white text-xs -mt-1">{item.content}</p>
      </div>
    </div>
  );
};

export default CardBody;
