import React, { useContext } from "react";
import { AddOrderMutation } from "../../services/Mutations";
import { Button } from "@material-ui/core";
import { MainContext } from "../../context/MainContext";

const AddOrderButton = ({ total }) => {
  const { mainState } = useContext(MainContext);
  const products = [];
  mainState.cart.forEach((item) => {
    products.push(item.id);
  });
  const price = parseInt(total);

  return (
    <AddOrderMutation>
      {({ handleAddOrder, loading }) => (
        <Button
          variant="contained"
          disabled={loading}
          className="my-5 w-100"
          color="primary"
          size="large"
          onClick={() => handleAddOrder(products, price)}
        >
          ثبت سفارش
        </Button>
      )}
    </AddOrderMutation>
  );
};

export default AddOrderButton;
