import React, { useContext } from "react";
import { MainContext } from "../../context/MainContext";
import { handlePersianNumber } from "../../utils/PersianNumber";
import { handleDecimalNumber } from "../../utils/DecimalNumber";
import AddOrderButton from "./AddOrderButton";
const TotalPrice = () => {
  const { mainState } = useContext(MainContext);
  let total = 0;
  mainState.cart.forEach((item) => {
    total += item.count * item.price;
  });

  return (
    <div className=" w-1/4 mx-auto py-5">
      <p className="text-center py-1 font-weight-bold">مجموع</p>
      <p className="text-center text-info font-weight-bold pb-2">
        {handlePersianNumber(handleDecimalNumber(total))} $
      </p>
      <AddOrderButton total={total} />
    </div>
  );
};

export default TotalPrice;
