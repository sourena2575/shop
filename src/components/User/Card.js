import React from "react";
import CardImage from "./CardImage";
import CardBody from "./CardBody";
import UserModal from "./Modal";

const Card = ({ item }) => {
  return (
    <div className="mx-auto my-3">
      <CardImage item={item} />
      <CardBody item={item} />
      <UserModal />
    </div>
  );
};

export default Card;
