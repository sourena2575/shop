import React, { useContext } from "react";
import PeopleAltIcon from "@material-ui/icons/PeopleAlt";
import { Badge } from "@material-ui/core";
import { MainContext } from "../../context/MainContext";
import { FollowButton } from "../Common/Button";

const CardBody = ({ item }) => {
  const { mainState } = useContext(MainContext);
  return (
    <div className=" bg-white w-52  h-24 mx-auto -mt-14 shadow relative rounded">
      <div className="flex justify-around">
        <Badge badgeContent={item.followerCount} className=" text-red-700 mt-4">
          <PeopleAltIcon className=" text-blue-700" />
        </Badge>
        <span className="text-base text-gray-700 font-bold pt-3 ">
          {item.userName}
        </span>
      </div>
      {mainState.user.userName !== item.userName && (
        <FollowButton item={item} />
      )}
    </div>
  );
};

export default CardBody;
