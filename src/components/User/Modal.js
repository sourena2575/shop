import React, { useContext, useEffect } from "react";
import { useLazyQuery } from "@apollo/react-hooks";
import { USER } from "../../services/queries/Authentication";
import { Dialog, DialogContent, DialogTitle } from "@material-ui/core";
import { MainContext } from "../../context/MainContext";
import ModalHeader from "./ModalHeader";
import ModalList from "./ModalList";
import { CircleProgress } from "../Common/Progress";
import CloseIcon from "@material-ui/icons/Close";

const UserModal = () => {
  const { mainState, mainDispatch } = useContext(MainContext);
  const [getUser, { loading, called, data }] = useLazyQuery(USER, {
    variables: { userId: mainState.selectedUser && mainState.selectedUser },
  });
  useEffect(() => {
    if (mainState.selectedUser) {
      getUser();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mainState.selectedUser]);

  return (
    <Dialog
      open={mainState.userModal}
      onClose={() => mainDispatch({ type: "USER_MODAL" })}
    >
      <DialogTitle id="alert-dialog-slide-title">
        {called && loading ? (
          <div className="flex justify-center align-middle">
            <CircleProgress color="secondary" />
          </div>
        ) : (
          <div className="flex flex-row-reverse justify-between">
            <p className="text-right text-black font-bold pr-5 pt-2">
              {data && data.getUser.userName}
            </p>
            <CloseIcon
              className=" mt-2"
              onClick={() => mainDispatch({ type: "USER_MODAL" })}
              style={{ cursor: "pointer", fontSize: 20 }}
            />
          </div>
        )}
      </DialogTitle>
      <DialogContent>
        {data && (
          <>
            <ModalHeader item={data.getUser} />
            <ModalList />
          </>
        )}
      </DialogContent>
    </Dialog>
  );
};

export default UserModal;
