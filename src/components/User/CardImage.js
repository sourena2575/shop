import React, { useContext } from "react";
import { MainContext } from "../../context/MainContext";
import Image from "../../static/Image/logo512.png";
const CardImage = ({ item }) => {
  const { mainDispatch } = useContext(MainContext);
  return (
    <div className="mx-auto">
      <img
        src={item.profile.image ? item.profile.image : Image}
        className=" w-56 h-32 object-cover object-center rounded"
        alt="تصویر"
        onClick={() => {
          mainDispatch({ type: "USER_MODAL", payload: item.id });
        }}
      />
    </div>
  );
};

export default CardImage;
