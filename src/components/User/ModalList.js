import React, { useContext } from "react";
import { useQuery } from "@apollo/react-hooks";
import { FOLLOWERS } from "../../services/queries/Authentication";
import { MainContext } from "../../context/MainContext";

const ModalList = () => {
  const { mainState } = useContext(MainContext);
  const { data, loading, error } = useQuery(FOLLOWERS, {
    variables: { userId: mainState.selectedUser && mainState.selectedUser },
  });

  if (error || loading) {
    return <div className="my-3"></div>;
  } else if (data.getFollowers.length === 0) {
    return <div className="my-3"></div>;
  }
  return (
    <div className="w-64 border rounded my-3">
      {data &&
        data.getFollowers.map((item, index) => {
          return (
            <div
              key={item.id}
              className={
                index === data.getFollowers.length - 1
                  ? "py-3 flex flex-row-reverse justify-between"
                  : "py-3 flex flex-row-reverse justify-between border-b"
              }
            >
              <div className="flex flex-col">
                <span className="font-bold text-teal-600 text-right pt-1 pr-4 text-sm">
                  {item.userName}
                </span>
                <span className=" text-xs text-gray-600 text-right pt-1 pr-4">
                  {item.name} {item.lastName}
                </span>
              </div>
              <img
                src={item.profile.image && item.profile.image}
                className=" h-14  w-14 rounded-full ml-3"
                alt="تصویر"
              />
            </div>
          );
        })}
    </div>
  );
};

export default ModalList;
