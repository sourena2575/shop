import React from "react";

const ModalHeader = ({ item }) => {
  return (
    <img
      src={item && item.profile.image}
      className="  w-64 h-48 rounded object-center object-cover"
      alt="تصویر"
    />
  );
};

export default ModalHeader;
