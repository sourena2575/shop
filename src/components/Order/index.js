import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { ORDERS } from "../../services/queries/Order";
import Card from "./Card.tsx";

const Index = () => {
  const { data, error, loading } = useQuery(ORDERS);
  if (loading || error) return null;
  return (
    <>
      <p className="h5 text-center text-muted pb-3 pt-5">سفارشات</p>
      <div className="grid grid-cols-1">
        {data.getOrders.map((item) => {
          return <Card key={item.id} item={item} />;
        })}
      </div>
    </>
  );
};

export default Index;
