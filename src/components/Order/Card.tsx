import React from "react";
import CardDetails from "./CardDetails";
import CardIcons from "./CardIcons";

interface Props {
  item: {
    id: string;
    price: number;
    createdAt: string;
    status: string;
    payed: boolean;
    products: string[];
  };
}
const Card: React.FC<Props> = ({ item }) => {
  return (
    <div className="border shadow rounded bg-light my-2 d-flex flex-row justify-content-between p-3">
      <CardDetails item={item} />
      <CardIcons item={item} />
    </div>
  );
};

export default Card;
