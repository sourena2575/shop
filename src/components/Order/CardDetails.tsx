import React from "react";
import moment from "moment";
import { handlePersianNumber } from "../../utils/PersianNumber";
import { handleDecimalNumber } from "../../utils/DecimalNumber";
interface Props {
  item: {
    id: string;
    price: number;
    createdAt: string;
    status: string;
    payed: boolean;
    products: string[];
  };
}
const CardDetails: React.FC<Props> = ({ item }) => {
  return (
    <div className="d-flex flex-column">
      <span className="text-info" style={{ fontSize: 15 }}>
        {item.id}
      </span>
      <span className="badge badge-info ml-3">{item.products.length}</span>
      <span className="text-left pl-3 text-primary">
        $ {handlePersianNumber(handleDecimalNumber(item.price))}
      </span>

      <span className="text-muted pl-3" style={{ fontSize: 12 }}>
        {moment(item.createdAt).fromNow()}
      </span>
    </div>
  );
};

export default CardDetails;
