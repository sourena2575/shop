import React from "react";
import { Link } from "react-router-dom";
import VisibilityIcon from "@material-ui/icons/Visibility";
import {
  DelOrdersButton,
  PayOrderButton,
  SendOrderButton,
} from "../../components/Common/Button";
interface Props {
  item: {
    payed: boolean;
    status: string;
    id: string;
  };
}
const CardIcons: React.FC<Props> = ({ item }) => {
  return (
    <div className="d-flex flex-column">
      <Link to={"/orders/" + item.id}>
        <VisibilityIcon className="text-secondary" />
      </Link>
      <PayOrderButton item={item} />
      <SendOrderButton item={item} />
      <DelOrdersButton orderId={item.id} />
    </div>
  );
};

export default CardIcons;
