export const FormData = [
  {
    name: "phone",
    label: "تلفن همراه  ",
    placeholder: "شماره تلفن همراه خود را وارد کنید",
  },
  {
    name: "idCode",
    label: "کد ملی ",
    placeholder: "کد ملی  خود را وارد کنید",
  },
  {
    name: "postCode",
    label: "کد پستی  ",
    placeholder: "کد پستی  خود را وارد ",
  },
  {
    name: "address",
    label: "آدرس ",
    placeholder: "آدرس  خود را وارد کنید",
    textArea: true,
  },
];
