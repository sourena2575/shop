import React, { useContext } from "react";
import { Drawer } from "@material-ui/core";

import { MainContext } from "../../context/MainContext";
import { DrawerForm } from "../Common/Form";
import { FormData } from "./FormData";
import { CreateProfileMutation } from "../../services/Mutations";

const ProfileDrawer = () => {
  const { mainDispatch, mainState } = useContext(MainContext);
  return (
    <Drawer
      className="text-center"
      anchor="left"
      onClose={() => mainDispatch({ type: "PROFILE_MODAL" })}
      open={mainState.profileModal}
    >
      <CreateProfileMutation>
        {({ handleCreate, loading }) => (
          <DrawerForm
            data={FormData}
            mutation={handleCreate}
            loading={loading}
            close={() => mainDispatch({ type: "PROFILE_MODAL" })}
            upload
            buttonText="تایید"
            title="تکمیل پروفایل"
          />
        )}
      </CreateProfileMutation>
    </Drawer>
  );
};

export default ProfileDrawer;
