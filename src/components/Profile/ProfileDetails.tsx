import React from "react";
import Card from "./Card";
import Image from "../../static/Image/logo512.png";
import Order from "../../components/Order";
interface Props {
  item: any;
}
const ProfileDetails: React.FC<Props> = ({ item }) => {
  return (
    <div className="grid sm:grid-cols-1 md:grid-cols-3 gap-3">
      <div className=" col-span-1">
        <img
          className="w-full h-64 rounded shadow"
          src={item.profile.image ? item.profile.image : Image}
          alt="محصول"
        />
      </div>
      <div className=" col-span-1 md:col-span-2">
        <Card item={item} />
        <Order />
      </div>
    </div>
  );
};

export default ProfileDetails;
