import ProfileDrawer from "./Drawer";
import ProfileDetails from "./ProfileDetails.tsx";

export { ProfileDrawer, ProfileDetails };
