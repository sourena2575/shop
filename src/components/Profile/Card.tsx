import React, { useContext } from "react";
import { MainContext } from "../../context/MainContext";
interface Props {
  item: any;
}
const DetailsCard: React.FC<Props> = ({ item }) => {
  const { mainDispatch } = useContext(MainContext);

  return (
    <div className="border rounded-lg shadow p-2 bg-light h-64">
      <div className="d-flex flex-row justify-content-around pt-3 h-16">
        <button
          className=" bg-indigo-800 rounded-sm hover:bg-teal-500 text-gray-200 text-xs shadow p-2 h-8"
          onClick={() => mainDispatch({ type: "PROFILE_MODAL" })}
        >
          تکمیل پروفایل
        </button>
        <span className="text-center text-xl text-red-700">
          {item.userName}
        </span>
      </div>

      <div className="d-flex flex-row justify-content-around py-2">
        <span className="pr-5 font-weight-bold text-teal-600">
          {item.profile.phone}
        </span>
        <span className="">:شماره تلفن</span>
      </div>

      <div className="d-flex flex-row justify-content-around py-2">
        <span className="pr-5 font-weight-bold text-teal-600">
          {item.profile.idCode}
        </span>
        <span className="">:کد ملی </span>
      </div>

      <div className="d-flex flex-row justify-content-around py-2">
        <span className="pr-5 font-weight-bold text-teal-600">
          {item.profile.postCode}
        </span>
        <span className="">:کد پستی </span>
      </div>

      <div className="d-flex flex-row justify-content-around py-2">
        <span className="pr-5 text-teal-600">{item.profile.address}</span>
      </div>
    </div>
  );
};

export default DetailsCard;
