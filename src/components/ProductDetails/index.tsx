import React from "react";
import DetailsColumn from "./DetailsColumn";
import DetailButtons from "./DetailButtons";
import CommentModal from "../Comment/CommentProduct/CommentModal";
interface Props {
  item: any;
}
const Index: React.FC<Props> = ({ item }) => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-10 col-sm-10 col-md-6 py-3 mx-auto">
          <img
            className="img-fluid rounded"
            src={item.image}
            alt="محصول"
            style={{ maxHeight: 300 }}
          />
        </div>
        <div className="col-12 col-sm-12 col-md-6">
          <DetailsColumn item={item} />
          <div className=" mx-3 mt-3">
            <p className="text-center pl-3 font-weight-bold">{item.desc} </p>
          </div>
        </div>
      </div>
      <DetailButtons item={item} />
      <CommentModal />
    </div>
  );
};

export default Index;
