import React from "react";
interface Props {
  right: string;
  left: any;
}
const DetailRows: React.FC<Props> = ({ right, left }) => {
  return (
    <div className="d-flex flex-row-reverse justify-content-between my-3">
      <span className="text-right">{right}</span>
      <span className="text-left pl-3 font-weight-bold">{left} </span>
    </div>
  );
};

export default DetailRows;
