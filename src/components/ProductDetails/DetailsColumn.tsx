import React from "react";

import DetailRows from "./DetailRows";

interface Props {
  item: {
    title: string;
    desc: string;
    price: number;
    color: string;
    company: string;
    size: string;
    number: number;
  };
}

const DetailsColumn: React.FC<Props> = ({ item }) => {
  const data = [
    { right: "شرکت سازنده", left: item.company },
    { right: "رنگ ", left: item.color },
    { right: "اندازه ", left: item.size },
    { right: "تعداد موجود در انبار ", left: item.number },
    { right: "قیمت", left: item.price },
  ];
  return (
    <div className="d-flex flex-column px-3 pt-4">
      {data.map((it, index) => {
        return <DetailRows right={it.right} left={it.left} key={index} />;
      })}
    </div>
  );
};

export default DetailsColumn;
