import React, { FC } from "react";
import { LikeIcon, AddCommentIcon, AddCartIcon } from "../Common/Button";
interface Props {
  item: any;
}
const DetailButtons: FC<Props> = ({ item }) => {
  return (
    <div className="flex flex-row justify-around my-4 w-1/2 mx-auto">
      <LikeIcon item={item} post={false} />
      <AddCommentIcon item={item} />
      <AddCartIcon item={item} />
    </div>
  );
};

export default DetailButtons;
