import ApolloClient from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { createHttpLink } from "apollo-link-http";
import { setContext } from "apollo-link-context";

const user = JSON.parse(localStorage.getItem("user"));

const httpLink = createHttpLink({
  uri: "https://api-graph-shop.herokuapp.com/",
});
const authLink = setContext(() => {
  if (user) {
    return {
      headers: {
        Authorization: `${user.token}`,
      },
    };
  } else {
    return "";
  }
});
export const client = new ApolloClient({
  link: user ? authLink.concat(httpLink) : httpLink,
  cache: new InMemoryCache(),
});
