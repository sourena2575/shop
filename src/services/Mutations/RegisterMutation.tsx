import { useMutation } from "@apollo/react-hooks";
import { SIGNUP } from "../queries/Authentication";
import { MainContext } from "../../context/MainContext";
import { useContext } from "react";

interface Props {
  children: any;
}

const RegisterMutation: React.FC<Props> = ({ children }) => {
  const { mainDispatch } = useContext(MainContext);

  const [signup, { loading }] = useMutation(SIGNUP, {
    update(_, result) {
      localStorage.setItem("user", JSON.stringify(result.data.signup));
      mainDispatch({
        type: "AUTH_REFRESH",
      });
    },
    onError(err: any) {
      mainDispatch({
        type: "AUTH_ERROR",
        payload: err.graphQLErrors[0]
          ? err.graphQLErrors[0].extensions.errors.msg
          : "مشکلی پیش آمده است، لطفا دوباره تلاش کنید",
      });
      console.log(err);
    },
  });

  const handleSub = (e: any) => {
    e.preventDefault();
    const name = e.target.elements.name.value;
    const lastName = e.target.elements.lastName.value;
    const email = e.target.elements.email.value;
    const userName = e.target.elements.userName.value;
    const password = e.target.elements.password.value;
    const password1 = e.target.elements.password1.value;
    if (password !== password1) {
      mainDispatch({
        type: "AUTH_ERROR",
        payload: "رمز های وراد شده متفاوت اند",
      });
    } else {
      signup({ variables: { name, lastName, email, userName, password } });
      mainDispatch({
        type: "AUTH_ERROR",
        payload: null,
      });
    }
  };

  return children({ handleSub: handleSub, loading: loading });
};

export default RegisterMutation;
