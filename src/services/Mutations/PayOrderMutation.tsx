import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { PAY, ORDERS } from "../queries/Order";

interface Props {
  children: any;
}
const PayOrderMutation: React.FC<Props> = ({ children }) => {
  const [payCart, { loading }] = useMutation(PAY, {
    update(proxy, result) {},
    onError(err) {
      console.log(err);
    },
  });
  const handlePay = async (orderId: string) => {
    await payCart({
      variables: { orderId },
      refetchQueries: [{ query: ORDERS }],
    });
  };
  return children({ handlePay: handlePay, loading: loading });
};

export default PayOrderMutation;
