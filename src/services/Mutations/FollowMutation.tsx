import { FC } from "react";
import { useMutation } from "@apollo/react-hooks";
import { FOLLOW, USERS } from "../queries/Authentication";
interface Props {
  children: any;
}
const FollowMutation: FC<Props> = ({ children }) => {
  const [addFollower, { loading }] = useMutation(FOLLOW, {
    update() {},
    onError(er) {
      console.log(er);
    },
  });
  const handleFollow = (userId: string) => {
    addFollower({ variables: { userId }, refetchQueries: [{ query: USERS }] });
  };
  return children({ handleFollow: handleFollow, loading: loading });
};

export default FollowMutation;
