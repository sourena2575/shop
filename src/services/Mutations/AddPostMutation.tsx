import React, { useContext } from "react";
import { useMutation } from "@apollo/react-hooks";
import { MainContext } from "../../context/MainContext";
import { CREATE_POST, POSTS } from "../queries/Posts";

interface Props {
  children: any;
}

const AddProductMutation: React.FC<Props> = ({ children }) => {
  const { mainState, mainDispatch } = useContext(MainContext);
  const [addPost, { loading }] = useMutation(CREATE_POST, {
    update(proxy, result) {
      // console.log(result);
      mainDispatch({ type: "ADD_POST_MODAL" });
    },
    onError(err) {
      console.log(err);
    },
  });
  const handleCreatePost = (e: any) => {
    e.preventDefault();
    const content = e.target.elements.content.value;
    const image = mainState.uploadImage;
    addPost({
      variables: { content, image },
      refetchQueries: [{ query: POSTS }],
    });
  };
  return children({
    loading: loading,
    handleCreatePost: handleCreatePost,
  });
};

export default AddProductMutation;
