import { useMutation } from "@apollo/react-hooks";

import { LIKEPRODUCT } from "../queries/Products";

interface Props {
  children: any;
}

const LikeProductMutation: React.FC<Props> = ({ children }) => {
  const [likeProduct, { loading }] = useMutation(LIKEPRODUCT, {
    update(_, result) {
      //console.log(result);
    },
    onError(err) {
      console.log(err);
    },
  });
  const handleLike = (productId: string) => {
    likeProduct({ variables: { productId } });
  };
  return children({ handleLike: handleLike, loading: loading });
};

export default LikeProductMutation;
