import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { CREATE_COMMENT, PRODUCTS } from "../queries/Products";

interface Props {
  children: any;
}

const CreateCommentMutation: React.FC<Props> = ({ children }) => {
  const [createComment, { loading }] = useMutation(CREATE_COMMENT, {
    update(proxy, result) {
      //console.log(result);
    },
    onError(err) {
      console.log(err);
    },
  });

  const handleComment = async (productId: string, body: string) => {
    await createComment({
      variables: { body, productId },
      refetchQueries: [{ query: PRODUCTS }],
    });
  };
  return children({ handleComment: handleComment, loading: loading });
};

export default CreateCommentMutation;
