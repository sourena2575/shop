import React from "react";
import { useMutation } from "@apollo/react-hooks";

import { ADDCART, CART } from "../queries/Cart";

interface Props {
  children: any;
}

const AddToCartMutation: React.FC<Props> = ({ children }) => {
  const [addToCart, { loading }] = useMutation(ADDCART, {
    update(_, result) {},
    onError(err) {
      console.log(err);
    },
  });
  const handleAddCart = (productId: string, userId: string) => {
    addToCart({
      variables: { productId, userId },
      refetchQueries: [{ query: CART, variables: { userId } }],
    });
  };
  return children({ handleAddCart: handleAddCart, loading: loading });
};

export default AddToCartMutation;
