import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_CART, CART } from "../queries/Cart";

interface Props {
  children: any;
}

const DeleteCartMutation: React.FC<Props> = ({ children }) => {
  const [deleteFromCart, { loading }] = useMutation(DELETE_CART, {
    update(proxy, result) {
      //console.log(result);
    },
    onError(err) {
      console.log(err);
    },
  });

  const handleDelete = async (productId: string, userId: string) => {
    await deleteFromCart({
      variables: { productId, userId },
      refetchQueries: [{ query: CART, variables: { userId } }],
    });
  };
  return children({ handleDelete: handleDelete, loading: loading });
};

export default DeleteCartMutation;
