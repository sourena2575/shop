import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_ORDER, ORDER } from "../queries/Order";

interface Props {
  children: any;
}

const DeleteOrderMutation: React.FC<Props> = ({ children }) => {
  const [removeCart, { loading }] = useMutation(DELETE_ORDER, {
    update(proxy, result) {
      //console.log(result);
    },
    onError(err) {
      console.log(err);
    },
  });

  const handleDelete = async (productId: string, orderId: string) => {
    await removeCart({
      variables: { productId, orderId },
      refetchQueries: [{ query: ORDER, variables: { orderId } }],
    });
  };
  return children({ handleDelete: handleDelete, loading: loading });
};

export default DeleteOrderMutation;
