import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_COMMENT, PRODUCTS } from "../queries/Products";

interface Props {
  children: any;
}

const DeleteCommentMutation: React.FC<Props> = ({ children }) => {
  const [deleteComment, { loading }] = useMutation(DELETE_COMMENT, {
    update(proxy, result) {
      //console.log(result);
    },
    onError(err) {
      console.log(err);
    },
  });

  const handleDelete = async (productId: string, commentId: string) => {
    await deleteComment({
      variables: { commentId, productId },
      refetchQueries: [{ query: PRODUCTS }],
    });
  };
  return children({ handleDelete: handleDelete, loading: loading });
};

export default DeleteCommentMutation;
