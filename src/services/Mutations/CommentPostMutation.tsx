import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { COMMENT_POST, POSTS } from "../queries/Posts";

interface Props {
  children: any;
}

const CreateCommentMutation: React.FC<Props> = ({ children }) => {
  const [commentPost, { loading }] = useMutation(COMMENT_POST, {
    update(proxy, result) {
      //console.log(result);
    },
    onError(err) {
      console.log(err);
    },
  });

  const handleComment = async (postId: string, body: string) => {
    console.log(postId, body);

    await commentPost({
      variables: { postId, body },
      refetchQueries: [{ query: POSTS }],
    });
  };
  return children({ handleComment: handleComment, loading: loading });
};

export default CreateCommentMutation;
