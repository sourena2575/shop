import { useMutation } from "@apollo/react-hooks";
import { LOGIN } from "../queries/Authentication";
import { MainContext } from "../../context/MainContext";
import { useContext } from "react";

interface Props {
  children: any;
}

const LoginMutation: React.FC<Props> = ({ children }) => {
  const { mainDispatch } = useContext(MainContext);

  const [login, { loading }] = useMutation(LOGIN, {
    update(_, result) {
      localStorage.setItem("user", JSON.stringify(result.data.login));
      mainDispatch({
        type: "AUTH_REFRESH",
      });
    },
    onError(err: any) {
      mainDispatch({
        type: "AUTH_ERROR",
        payload: err.graphQLErrors[0]
          ? err.graphQLErrors[0].extensions.errors.msg
          : "مشکلی پیش آمده است، لطفا دوباره تلاش کنید",
      });
      console.log(err);
    },
  });
  const handleSub = (e: any) => {
    e.preventDefault();
    const userName = e.target.elements.userName.value;
    const password = e.target.elements.password.value;
    login({ variables: { userName, password } });
  };
  return children({ handleSub: handleSub, loading: loading });
};

export default LoginMutation;
