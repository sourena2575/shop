import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { SEND, ORDERS } from "../queries/Order";
interface Props {
  children: any;
}
const SendOrderMutation: React.FC<Props> = ({ children }) => {
  const [sendCart, { loading }] = useMutation(SEND, {
    update() {},
    onError(err) {
      console.log(err);
    },
  });
  const handleSend = (orderId: string) => {
    sendCart({ variables: { orderId }, refetchQueries: [{ query: ORDERS }] });
  };
  return children({ handleSend: handleSend, loading: loading });
};

export default SendOrderMutation;
