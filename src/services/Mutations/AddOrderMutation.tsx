import React, { useContext } from "react";
import { useMutation } from "@apollo/react-hooks";

import { ADD_ORDER, ORDERS } from "../queries/Order";
import { MainContext } from "../../context/MainContext";
import { useHistory } from "react-router-dom";

interface Props {
  children: any;
}
const AddOrderMutation: React.FC<Props> = ({ children }) => {
  const { mainDispatch } = useContext(MainContext);
  const history = useHistory();
  const [addOrder, { loading }] = useMutation(ADD_ORDER, {
    update() {
      history.push("/profile");
      mainDispatch({ type: "CLEAR_CART" });
    },
    onError(er) {
      console.log(er);
    },
  });
  const handleAddOrder = (products: [], price: number) => {
    addOrder({
      variables: { products, price },
      refetchQueries: [{ query: ORDERS }],
    });
  };
  return children({ handleAddOrder: handleAddOrder, loading: loading });
};

export default AddOrderMutation;
