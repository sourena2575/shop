import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_COMMENT_POST, POSTS } from "../queries/Posts";

interface Props {
  children: any;
}

const DeleteCommentMutation: React.FC<Props> = ({ children }) => {
  const [deleteCommentPost, { loading }] = useMutation(DELETE_COMMENT_POST, {
    update(proxy, result) {
      //console.log(result);
    },
    onError(err) {
      console.log(err);
    },
  });

  const handleDelete = async (postId: string, commentId: string) => {
    await deleteCommentPost({
      variables: { postId, commentId },
      refetchQueries: [{ query: POSTS }],
    });
  };
  return children({ handleDelete: handleDelete, loading: loading });
};

export default DeleteCommentMutation;
