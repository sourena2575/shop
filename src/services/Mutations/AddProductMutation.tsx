import React, { useContext } from "react";
import { useMutation } from "@apollo/react-hooks";
import { CREATE_PRODUCT, PRODUCTS } from "../queries/Products";
import { MainContext } from "../../context/MainContext";

interface Props {
  children: any;
}

const AddProductMutation: React.FC<Props> = ({ children }) => {
  const { mainState, mainDispatch } = useContext(MainContext);
  const [createProduct, { loading }] = useMutation(CREATE_PRODUCT, {
    update(proxy, result) {
      // console.log(result);
      mainDispatch({ type: "ADD_PRODUCT_MODAL" });
    },
    onError(err) {
      console.log(err);
    },
  });
  const handleCreateProduct = (e: any) => {
    e.preventDefault();
    const price = parseInt(e.target.elements.price.value);
    const color = e.target.elements.color.value;
    const company = e.target.elements.company.value;
    const size = e.target.elements.size.value;
    const number = parseInt(e.target.elements.price.value);
    const desc = e.target.elements.desc.value;
    const title = e.target.elements.title.value;
    const image = mainState.uploadImage;
    createProduct({
      variables: { price, color, company, size, number, desc, title, image },
      refetchQueries: [{ query: PRODUCTS }],
    });
  };
  return children({
    loading: loading,
    handleCreateProduct: handleCreateProduct,
  });
};

export default AddProductMutation;
