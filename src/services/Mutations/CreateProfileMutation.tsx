import React, { useContext } from "react";
import { useMutation } from "@apollo/react-hooks";
import { CREATE_PROFILE, USER } from "../queries/Authentication";
import { MainContext } from "../../context/MainContext";

interface Props {
  children: any;
}

const CreateProfileMutation: React.FC<Props> = ({ children }) => {
  const { mainState, mainDispatch } = useContext(MainContext);
  const [createProfile, { loading }] = useMutation(CREATE_PROFILE, {
    update(proxy, result) {
      //console.log(result);
      mainDispatch({ type: "PROFILE_MODAL" });
    },
    onError(err) {
      console.log(err);
    },
  });

  const handleCreate = async (e: any) => {
    e.preventDefault();
    const address = e.target.elements.address.value;
    const phone = e.target.elements.phone.value;
    const idCode = e.target.elements.idCode.value;
    const postCode = e.target.elements.postCode.value;
    const userName = mainState.user && mainState.user.userName;
    const image = mainState.uploadImage;
    await createProfile({
      variables: { image, address, phone, idCode, postCode, userName },
      refetchQueries: [
        { query: USER, variables: { userId: mainState.user.id } },
      ],
    });
  };
  return children({ handleCreate: handleCreate, loading: loading });
};

export default CreateProfileMutation;
