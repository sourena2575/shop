import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_ORDERS, ORDERS } from "../queries/Order";

interface Props {
  children: any;
}

const DeleteOrdersMutation: React.FC<Props> = ({ children }) => {
  const [deleteOrder, { loading }] = useMutation(DELETE_ORDERS, {
    update(proxy, result) {
      //console.log(result);
    },
    onError(err) {
      console.log(err);
    },
  });

  const handleDelete = async (orderId: string) => {
    await deleteOrder({
      variables: { orderId },
      refetchQueries: [{ query: ORDERS }],
    });
  };
  return children({ handleDelete: handleDelete, loading: loading });
};

export default DeleteOrdersMutation;
