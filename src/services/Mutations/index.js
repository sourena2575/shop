import LikeProductMutation from "./LikeProductMutation.tsx";
import AddToCartMutation from "./AddToCartMutation.tsx";
import LoginMutation from "./LoginMutation.tsx";
import RegisterMutation from "./RegisterMutation.tsx";
import CreateCommentMutation from "./CreateCommentMutation.tsx";
import DeleteCommentMutation from "./DeleteCommentMutation.tsx";
import DeleteCartMutation from "./DeleteCartMutation.tsx";
import CreateProfileMutation from "./CreateProfileMutation.tsx";
import AddProductMutation from "./AddProductMutation.tsx";
import AddOrderMutation from "./AddOrderMutation.tsx";
import DeleteOrderMutation from "./DeleteOrderMutation.tsx";
import DeleteOrdersMutation from "./DeleteOrdersMutation.tsx";
import PayOrderMutation from "./PayOrderMutation.tsx";
import SendOrderMutation from "./SendOrderMutation.tsx";
import FollowMutation from "./FollowMutation.tsx";
import DeletePostMutation from "./DeletePostMutation.tsx";
import LikePostMutation from "./LikePostMutation.tsx";
import CommentPostMutation from "./CommentPostMutation.tsx";
import AddPostMutation from "./AddPostMutation.tsx";
import DeleteCommentPostMutation from "./DeleteCommentPostMutation.tsx";
export {
  LikeProductMutation,
  AddToCartMutation,
  LoginMutation,
  RegisterMutation,
  CreateCommentMutation,
  DeleteCommentMutation,
  DeleteCartMutation,
  CreateProfileMutation,
  AddProductMutation,
  AddOrderMutation,
  DeleteOrderMutation,
  DeleteOrdersMutation,
  PayOrderMutation,
  SendOrderMutation,
  FollowMutation,
  DeletePostMutation,
  LikePostMutation,
  CommentPostMutation,
  AddPostMutation,
  DeleteCommentPostMutation,
};
