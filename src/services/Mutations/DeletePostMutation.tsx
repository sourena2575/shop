import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_POST, POSTS } from "../queries/Posts";

interface Props {
  children: any;
}

const DeleteCommentMutation: React.FC<Props> = ({ children }) => {
  const [deletePost, { loading }] = useMutation(DELETE_POST, {
    update(proxy, result) {
      //console.log(result);
    },
    onError(err) {
      console.log(err);
    },
  });

  const handleDelete = async (postId: string) => {
    await deletePost({
      variables: { postId },
      refetchQueries: [{ query: POSTS }],
    });
  };
  return children({ handleDelete: handleDelete, loading: loading });
};

export default DeleteCommentMutation;
