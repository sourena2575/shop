import { useMutation } from "@apollo/react-hooks";
import { LIKE_POST, POSTS } from "../queries/Posts";

interface Props {
  children: any;
}

const LikeProductMutation: React.FC<Props> = ({ children }) => {
  const [likePost, { loading }] = useMutation(LIKE_POST, {
    update(_, result) {
      //console.log(result);
    },
    onError(err) {
      console.log(err);
    },
  });
  const handleLike = (postId: string) => {
    likePost({ variables: { postId }, refetchQueries: [{ query: POSTS }] });
  };
  return children({ handleLike: handleLike, loading: loading });
};

export default LikeProductMutation;
