import gql from "graphql-tag";

export const PRODUCTS = gql`
  {
    getProducts {
      id
      userName
      image
      price
      color
      company
      size
      number
      createdAt
      commentCount
      likeCount
      desc
      title
      comments {
        id
        body
        userName
        createdAt
      }
      likes {
        id
        userName
        createdAt
      }
    }
  }
`;

export const PRODUCT = gql`
  query($productId: ID!) {
    getProduct(productId: $productId) {
      id
      userName
      image
      price
      color
      company
      size
      number
      createdAt
      commentCount
      likeCount
      desc
      title
      comments {
        id
        body
        userName
        createdAt
      }
      likes {
        id
        userName
        createdAt
      }
    }
  }
`;

export const LIKEPRODUCT = gql`
  mutation likeProduct($productId: ID!) {
    likeProduct(productId: $productId) {
      id
      likes {
        id
        userName
        createdAt
      }
      likeCount
    }
  }
`;

export const CREATE_COMMENT = gql`
  mutation createComment($productId: ID!, $body: String!) {
    createComment(productId: $productId, body: $body) {
      id
      comments {
        createdAt
        body
        userName
        id
      }
    }
  }
`;

export const DELETE_COMMENT = gql`
  mutation deleteComment($productId: ID!, $commentId: ID!) {
    deleteComment(productId: $productId, commentId: $commentId) {
      id
      comments {
        id
        userName
        body
      }
    }
  }
`;

export const deleteProductMutation = gql`
  mutation deleteProduct($productId: ID!) {
    deleteProduct(productId: $productId) {
      id
    }
  }
`;

export const CREATE_PRODUCT = gql`
  mutation createProduct(
    $image: String
    $price: Int
    $color: String
    $company: String
    $size: String
    $number: Int
    $desc: String
    $title: String
  ) {
    createProduct(
      image: $image
      price: $price
      color: $color
      company: $company
      size: $size
      number: $number
      desc: $desc
      title: $title
    ) {
      id
      image
    }
  }
`;
