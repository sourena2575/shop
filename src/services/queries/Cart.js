import gql from "graphql-tag";

export const CART = gql`
  query getCart($userId: ID!) {
    getCart(userId: $userId) {
      id
      title
      image
      price
      company
      desc
      size
      number
      color
    }
  }
`;

export const ADDCART = gql`
  mutation addToCart($productId: ID!, $userId: ID!) {
    addToCart(productId: $productId, userId: $userId) {
      id
      userName
      cart
    }
  }
`;

export const DELETE_CART = gql`
  mutation deleteFromCart($productId: ID!, $userId: ID!) {
    deleteFromCart(productId: $productId, userId: $userId) {
      id
      cart
    }
  }
`;
