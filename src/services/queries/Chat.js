import gql from "graphql-tag";

export const NEW_CHAT_SUBSCRIPTION = gql`
  subscription newMessage {
    newMessage {
      text
      user {
        userName
      }
      createdAt
    }
  }
`;

export const NEW_CHAT = gql`
  mutation createMessage($text: String, $channelId: String) {
    createMessage(text: $text, channelId: $channelId) {
      text
    }
  }
`;

export const CHATS = gql`
  {
    getMessages {
      id
      text
      user {
        userName
      }
      createdAt
      channelId
    }
  }
`;
