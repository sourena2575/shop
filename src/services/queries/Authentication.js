import gql from "graphql-tag";

export const SIGNUP = gql`
  mutation signup(
    $name: String!
    $lastName: String!
    $userName: String!
    $email: String!
    $password: String!
  ) {
    signup(
      name: $name
      lastName: $lastName
      userName: $userName
      email: $email
      password: $password
    ) {
      id
      name
      lastName
      email
      userName
      token
    }
  }
`;

export const LOGIN = gql`
  mutation login($userName: String!, $password: String!) {
    login(userName: $userName, password: $password) {
      id
      name
      lastName
      email
      userName
      token
    }
  }
`;

export const USER = gql`
  query getUser($userId: ID!) {
    getUser(userId: $userId) {
      id
      userName
      email
      name
      lastName
      profile {
        image
        address
        phone
        idCode
        postCode
        userName
      }
    }
  }
`;

export const CREATE_PROFILE = gql`
  mutation createProfile(
    $image: String
    $address: String
    $phone: String
    $idCode: String
    $postCode: String
    $userName: String!
  ) {
    createProfile(
      image: $image
      address: $address
      phone: $phone
      idCode: $idCode
      postCode: $postCode
      userName: $userName
    ) {
      id
    }
  }
`;

export const USERS = gql`
  {
    getUsers {
      id
      userName
      email
      name
      lastName
      profile {
        image
        address
        phone
        idCode
        postCode
        userName
      }
      followers
      followerCount
    }
  }
`;

export const FOLLOW = gql`
  mutation addFollower($userId: ID!) {
    addFollower(userId: $userId) {
      id
    }
  }
`;

export const FOLLOWERS = gql`
  query getFollowers($userId: ID) {
    getFollowers(userId: $userId) {
      id
      name
      lastName
      userName
      email
      profile {
        image
      }
      followerCount
    }
  }
`;
