import gql from "graphql-tag";

export const POSTS = gql`
  {
    getPosts {
      id
      userName
      content
      image
      comments {
        id
        userName
        body
        createdAt
      }
      likes {
        userName
        id
        createdAt
      }
      commentCount
      likeCount
      updatedAt
      createdAt
    }
  }
`;

export const POST = gql`
  query getPost($postId: ID) {
    getPost(postId: $postId) {
      id
      userName
      content
      image
      comments {
        id
        userName
        body
        createdAt
      }
      likes {
        userName
        id
        createdAt
      }
      commentCount
      likeCount
      updatedAt
      createdAt
    }
  }
`;

export const CREATE_POST = gql`
  mutation addPost($content: String!, $image: String!) {
    addPost(content: $content, image: $image) {
      id
    }
  }
`;

export const DELETE_POST = gql`
  mutation deletePost($postId: ID!) {
    deletePost(postId: $postId) {
      id
    }
  }
`;

export const UPDATE_POST = gql`
  mutation updatePost($postId: ID, $image: String, $content: String) {
    updatePost(postId: $postId, image: $image, content: $content) {
      id
    }
  }
`;

export const LIKE_POST = gql`
  mutation likePost($postId: ID!) {
    likePost(postId: $postId) {
      id
    }
  }
`;

export const COMMENT_POST = gql`
  mutation commentPost($postId: ID!, $body: String!) {
    commentPost(postId: $postId, body: $body) {
      id
    }
  }
`;

export const DELETE_COMMENT_POST = gql`
  mutation deleteCommentPost($postId: ID!, $commentId: ID!) {
    deleteCommentPost(postId: $postId, commentId: $commentId) {
      id
    }
  }
`;
