import gql from "graphql-tag";

export const ADD_ORDER = gql`
  mutation addOrder($products: [ID], $price: Int!) {
    addOrder(products: $products, price: $price) {
      id
      userName
      products
      createdAt
      payed
      status
      price
    }
  }
`;

export const DELETE_ORDER = gql`
  mutation removeCart($productId: ID!, $orderId: ID!) {
    removeCart(productId: $productId, orderId: $orderId) {
      id
      products
    }
  }
`;

export const DELETE_ORDERS = gql`
  mutation deleteOrder($orderId: ID!) {
    deleteOrder(orderId: $orderId) {
      id
    }
  }
`;

export const ORDERS = gql`
  query getOrders {
    getOrders {
      products
      id
      payed
      status
      userName
      createdAt
      price
    }
  }
`;

export const ORDER = gql`
  query getOrder($orderId: ID) {
    getOrder(orderId: $orderId) {
      id
      title
      image
      price
      color
      company
      size
      number
      desc
      createdAt
      comments {
        id
        userName
        body
        createdAt
      }
      likes {
        id
        userName
        createdAt
      }
      commentCount
      likeCount
    }
  }
`;

export const PAY = gql`
  mutation payCart($orderId: ID!) {
    payCart(orderId: $orderId) {
      id
    }
  }
`;

export const SEND = gql`
  mutation($orderId: ID!) {
    sendCart(orderId: $orderId) {
      id
    }
  }
`;
