import React, { useContext } from "react";
import TotalPrice from "../../components/Cart/TotalPrice";
import { MainContext } from "../../context/MainContext";
import { Card } from "../../components/Common/ProductCard";

const Index = () => {
  const { mainState } = useContext(MainContext);
  return (
    <div className="container my-5">
      <p className="h5 text-gray-700 text-center py-3 ">
        {mainState.cart.length > 0 ? "سبد خرید" : "سبد خرید شما خالی است"}
      </p>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4">
        {mainState.cart.length > 0 &&
          mainState.cart.map((item) => {
            return <Card item={item} key={item.id} cart={true} />;
          })}
      </div>
      {mainState.cart.length > 0 && (
        <div style={{ height: 150 }}>
          <TotalPrice />
        </div>
      )}
    </div>
  );
};

export default Index;
