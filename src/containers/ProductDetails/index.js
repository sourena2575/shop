import React from "react";
import { useQuery } from "@apollo/react-hooks";

import { PRODUCT } from "../../services/queries/Products";
import { CircleProgress } from "../../components/Common/Progress";
import Details from "../../components/ProductDetails";
import { useParams } from "react-router-dom";

const Index = () => {
  const params = useParams();
  const { data, loading, error } = useQuery(PRODUCT, {
    variables: { productId: params.id },
  });

  if (loading || error) return <CircleProgress color="primary" center />;
  return (
    <div className="container my-5">
      <p className="h5 text-gray-700 text-center py-3 ">
        {data && data.getProduct.title}
      </p>
      <div className="row">
        <Details item={data && data.getProduct} />
      </div>
    </div>
  );
};

export default Index;
