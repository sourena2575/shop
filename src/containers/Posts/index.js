import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { POSTS } from "../../services/queries/Posts";
import { CircleProgress } from "../../components/Common/Progress";
import Card from "../../components/Post/Card/Card";
import { AddPostDrawer } from "../../components/Post/PostAdd";

const Index = () => {
  const { data, loading, error } = useQuery(POSTS);

  if (loading || error) {
    return <CircleProgress center color="primary" />;
  }
  return (
    <div className="container my-5">
      <p className="h5 text-gray-700 text-center py-3 ">پست ها</p>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4">
        {data.getPosts.map((item) => {
          return <Card item={item} key={item.id} />;
        })}
      </div>
      <AddPostDrawer />
    </div>
  );
};

export default Index;
