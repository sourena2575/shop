import React, { useContext } from "react";
import { useQuery } from "@apollo/react-hooks";
import { ProfileDrawer } from "../../components/Profile";
import { USER } from "../../services/queries/Authentication";
import { MainContext } from "../../context/MainContext";
import { ProfileDetails } from "../../components/Profile";
import { CircleProgress } from "../../components/Common/Progress";

const Index = () => {
  const { mainState } = useContext(MainContext);
  const { data, loading, error } = useQuery(USER, {
    variables: { userId: mainState.user && mainState.user.id },
  });

  if (loading || error) return <CircleProgress color="primary" center />;

  return (
    <div className="container my-5">
      <p className="h5 text-gray-700 text-center py-3  ">پروفایل</p>
      <div className="grid grid-cols-1">
        <ProfileDetails item={data.getUser} />
      </div>
      <ProfileDrawer />
    </div>
  );
};

export default Index;
