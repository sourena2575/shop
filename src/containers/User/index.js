import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { USERS } from "../../services/queries/Authentication";
import Card from "../../components/User/Card";
import { CircleProgress } from "../../components/Common/Progress";

const Index = () => {
  const { data, loading, error } = useQuery(USERS);

  if (loading || error) {
    return <CircleProgress color="primary" center />;
  }
  return (
    <div className="container my-5">
      <p className="h5 text-gray-700 text-center py-3 ">کاربران</p>
      <div className=" grid sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
        {data.getUsers.map((item) => {
          return <Card item={item} key={item.id} />;
        })}
      </div>
    </div>
  );
};

export default Index;
