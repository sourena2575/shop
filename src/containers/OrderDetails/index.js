import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { CircleProgress } from "../../components/Common/Progress";
import { ORDER } from "../../services/queries/Order";
import { Card } from "../../components/Common/ProductCard";
import CommentModal from "../../components/Comment/CommentProduct/CommentModal";

const Index = (props) => {
  const { data, loading, error } = useQuery(ORDER, {
    variables: {
      orderId: props.match.params.id,
    },
  });
  if (loading || error) return <CircleProgress color="primary" center />;
  return (
    <div className="container my-5">
      <p className="h5 text-gray-700 text-center py-3 ">
        محصولات موجود در سفارش
      </p>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4">
        {data.getOrder.map((item) => {
          return (
            <Card item={item} key={item.id} orderId={props.match.params.id} />
          );
        })}
      </div>
      <CommentModal />
    </div>
  );
};

export default Index;
