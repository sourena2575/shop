import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { PRODUCTS } from "../../services/queries/Products";
import { Card } from "../../components/Common/ProductCard";
import { CircleProgress } from "../../components/Common/Progress";
import CommentModal from "../../components/Comment/CommentProduct/CommentModal";
import { AddDrawer } from "../../components/Product/ProductAdd";

const Index = () => {
  const { data, loading, error } = useQuery(PRODUCTS);

  if (loading || error) return <CircleProgress color="primary" center />;
  return (
    <div className="container my-5">
      <p className="h5 text-gray-700 text-center py-3 ">محصولات</p>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4">
        {data.getProducts.map((item) => {
          return <Card item={item} key={item.id} />;
        })}
      </div>
      <CommentModal />
      <AddDrawer />
    </div>
  );
};

export default Index;
