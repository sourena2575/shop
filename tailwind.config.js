module.exports = {
  purge: [],
  theme: {
    extend: {
      spacing: {
        "14": "3.5rem",
        "34": "8.5rem",
        "38": "9rem",
        "42": "10.5rem",
        "44": "11rem",
        "46": "11.5rem",
        "50": "12.5rem",
        "52": "13rem",
        "54": "13.5rem",
        "60": "15rem",
        "64": "16rem",
        "70": "17rem",
        "74": "18rem",
        "76": "19rem",
      },
    },
  },
  variants: {},
  plugins: [],
};
